import React from 'react';

const NotFoundPage = () => {
  return (
    <main
      style={{
        height: '100vh',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
      }}
    >
      <h1>404</h1>
      <h2>Ups something went wrong...</h2>
      <h3>
        The page you are looking for either doesn't exist or can't be found.
      </h3>
    </main>
  );
};

export default NotFoundPage;
