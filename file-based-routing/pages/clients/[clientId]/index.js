import React from 'react';
import { useRouter } from 'next/router';

const ClientProjectPage = () => {
  const router = useRouter();
  const query = router.query;

  const loadProjectHandler = () => {
    // load data..

    router.push({
      pathname: '/clients/[clientId]/[clientProjectId]',
      query: { clientId: query.clientId, clientProjectId: 'projectA' },
    });
  };

  return (
    <main>
      <h1>The Projects of Client {query.clientId}</h1>
      <button onClick={loadProjectHandler}>Load Project A</button>
    </main>
  );
};

export default ClientProjectPage;
