import React from 'react';
import { useRouter } from 'next/router';

const SelectedClientProjectPage = () => {
  const router = useRouter();
  const query = router.query;

  return (
    <main>
      <h1>
        Project {query.clientProjectId} of Client {query.clientId}
      </h1>
    </main>
  );
};

export default SelectedClientProjectPage;
