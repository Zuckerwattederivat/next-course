import React from 'react';
import Link from 'next/link';

const ClientPage = () => {
  const clients = [
    { id: 1, name: 'Maximilian' },
    { id: 2, name: 'Brian' },
    { id: 3, name: 'Nils' },
    { id: 4, name: 'Daggi' },
  ];

  return (
    <main>
      <h1>The Clients Page</h1>
      <ul>
        {clients &&
          clients.map(client => (
            <li key={client.id}>
              <Link
                href={{
                  pathname: '/clients/[clientId]',
                  query: { clientId: client.id },
                }}
              >
                {client.name}
              </Link>
            </li>
          ))}
      </ul>
    </main>
  );
};

export default ClientPage;
