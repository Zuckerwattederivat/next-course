import React from 'react';

const PortfolioPage = () => {
  return (
    <main>
      <h1>Portfolio Page</h1>
      <p>This is the portfolio page.</p>
    </main>
  );
};

export default PortfolioPage;
