import React from 'react';
import { useRouter } from 'next/router';

const PortfolioProjectPage = () => {
  const query = useRouter().query;

  return (
    <main>
      <h1>Project {query.projectId}</h1>
    </main>
  );
};

export default PortfolioProjectPage;
