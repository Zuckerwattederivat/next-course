import React from 'react';

const ListPage = () => {
  return (
    <main>
      <h1>Portfolio List</h1>
      <ul>
        <li>Portfolio 1</li>
        <li>Portfolio 2</li>
        <li>Portfolio 3</li>
      </ul>
    </main>
  );
};

export default ListPage;
