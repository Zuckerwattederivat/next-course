import React from 'react';
import Link from 'next/link';

const Homepage = () => {
  return (
    <main>
      <h1>Homepage</h1>
      <p>This is the homepage of our next app.</p>
      <ul>
        <li>
          <Link href='/portfolio'>Portfolio</Link>
        </li>
        <li>
          <Link href='/clients'>Clients</Link>
        </li>
      </ul>
    </main>
  );
};

export default Homepage;
